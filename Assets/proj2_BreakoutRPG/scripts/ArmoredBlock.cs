using UnityEngine;
using System.Collections;

public class ArmoredBlock : BasicBlock {
	
	public Material unarmoredMaterial;
	
	protected override void OnAwake() {
		base.OnAwake();
		(HP as ArmoredHealthSystem).ArmorDamaged += OnArmorDamaged;
	}
	
	protected void OnArmorDamaged(int damage, int currentArmor) {
		if (currentArmor <= 0) {
			renderer.material = unarmoredMaterial;	
		}
	}
}
