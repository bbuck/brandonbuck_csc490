using UnityEngine;
using System.Collections;

public class ArmoredHealthSystem : HealthSystem {
	
	public delegate void ArmorDamagedHandler(int damage, int currentArmor);
	public event ArmorDamagedHandler ArmorDamaged;
	
	public int maxArmor = 1;
	
	public override int TotalHealth {
		get {
			return maxHealth + maxArmor;
		}
	}
	
	public int CurrentArmor {get; private set;}
	
	#region event functions
	
	protected void OnArmorDamaged(int damage, int currentArmor) {
		if (ArmorDamaged != null) {
			ArmorDamaged(damage, currentArmor);
		}
	}
	
	#endregion event functions
	
	#region helper functions
	
	protected override void OnAwake() {
		base.OnAwake();
		CurrentArmor = maxArmor;
	}
	
	#endregion helper functions
	
	#region public functions
	
	public override bool IsDead() {
		return CurrentArmor <= 0 && CurrentHealth <= 0;
	}
	
	public override void Damage(int amount) {
		if (CurrentArmor > 0) {
			CurrentArmor -= amount;
			if (CurrentArmor < 0)
				CurrentArmor = 0;
			OnArmorDamaged(amount, CurrentArmor);
		}
		else
			CurrentHealth -= amount;
		OnHealthChanged(amount, CurrentArmor + CurrentHealth);
	}
	
	#endregion public functions
}
