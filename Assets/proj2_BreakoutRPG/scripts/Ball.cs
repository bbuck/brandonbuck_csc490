using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	
	public float initialSpeed = 10;
	public float maxSpeed = 15;
	public float speedIncrement = 0.1f;
	public int damage = 1;
	
	private float currentSpeed;
	
	// Use this for initialization
	void Start() {
		SetVelocity(initialSpeed);
	}
	
	// Update is called once per frame
	void Update() {
		CheckBounds();
		CheckSpeed();
	}
	
	void OnCollisionEnter(Collision collision) {
		HandleCollision(collision);
	}
	
	#region Helper functions
	
	protected virtual void CheckBounds() {
		Vector3 velocity = rigidbody.velocity;
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		
		if (topRight.x > 1) {
			velocity.x = -Mathf.Abs(velocity.x);
			IncrementSpeed();
		}
		else if (bottomLeft.x < 0) {
			velocity.x = Mathf.Abs(velocity.x);
			IncrementSpeed();
		}
		else if (topRight.y > 1) {
			velocity.y = -Mathf.Abs(velocity.y);
			IncrementSpeed();
		}
		else if (bottomLeft.y < 0)
			HandleDeadZoneCollision();
		
		rigidbody.velocity = velocity;
	}
	
	protected virtual void HandleDeadZoneCollision() {
		Destroy(gameObject);
	}
	
	void CheckSpeed() {
		Vector3 velocity = rigidbody.velocity;
		
		if (velocity.magnitude < currentSpeed)
			SetVelocity(currentSpeed);
	}
	
	void SetVelocity(float newSpeed) {
		Vector3 velocity = rigidbody.velocity;
		if (velocity.magnitude == 0)
			velocity = RandomVelocity();
		velocity.Normalize();
		rigidbody.velocity = velocity * newSpeed;
		currentSpeed = newSpeed;
	}
	
	void IncrementSpeed() {
		if (currentSpeed >= maxSpeed) return;
		currentSpeed += speedIncrement;
	}
	
	internal void HandleCollision(Collision collision) {
		if (collision.gameObject.CompareTag("block")) {
			collision.gameObject.GetComponent<BasicBlock>().Damage(damage);
		}
		
		IncrementSpeed();
	}
	
	Vector3 RandomVelocity() {
		float xVelocity = Random.Range(-0.35f, 0.35f);
		return new Vector3(xVelocity, 1, 0);
	}
		
	#endregion Helper functions
}
