using UnityEngine;
using System.Collections;

public class BasicBlock : MonoBehaviour {
	
	public HealthSystem HP {get; private set;}
	
	void Awake() {
		OnAwake();
	}
	
	#region helper methods
	
	protected virtual void OnAwake() {
		HP = GetComponent<HealthSystem>();
		HP.HealthChanged += OnHealthChanged;	
	}
	
	#endregion helper methods
	
	#region public methods
	
	public void Remove() {
		Vector3 position = transform.position;
		Destroy(gameObject);
		GameManager.Instance.AddScore(HP.TotalHealth);
		GameManager.Instance.CreateExplosion(position);
		GameManager.Instance.SpawnPowerup(position);
	}
	
	public void Damage(int amount) {
		HP.Damage(amount);	
	}
	
	public void OnHealthChanged(int amount, int currentHealth) {
		if (amount != 0)
			GameManager.Instance.CreateDamageText(amount, transform.position);
		if (currentHealth <= 0)
			Remove();
	}
	
	#endregion public methods
}
