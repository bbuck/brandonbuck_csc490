using UnityEngine;
using System.Collections;

public class BasicPowerup : MonoBehaviour {
	protected string notification;
	
	void OnTriggerEnter(Collider collider) {
		if (collider.gameObject.CompareTag("paddle")) {
			ApplyEffect();
			GameManager.Instance.Notify(notification);
			Destroy(transform.parent.gameObject);
		}
	}
	
	void Update() {
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		
		if (topRight.y < 0)
			Destroy(transform.parent.gameObject);
	}
	
	protected virtual void ApplyEffect() {
		print("Not implemented");
	}
}
