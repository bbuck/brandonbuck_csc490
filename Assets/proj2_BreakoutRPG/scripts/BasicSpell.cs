using UnityEngine;
using System.Collections;

public class BasicSpell {
	
	public string Name {get; private set;}
	public float CooldownTime {get; private set;}
	public float ManaCost {get; private set;}
	
	private float nextCastable;
	
	public BasicSpell(string name, float cooldownTime, float manaCost) {
		Name = name;
		CooldownTime = cooldownTime;
		ManaCost = manaCost;
		nextCastable = 0;
	}
	
	public bool IsCastable(float currentTime, float manaValue) {
		return (currentTime > nextCastable && manaValue >= ManaCost);
	}
	
	public void Cast(float currentTime) {
		nextCastable = currentTime + CooldownTime;
		if (Player.Instance.Mana.Remove(ManaCost))
			SpawnComponents();
	}
	
	protected virtual void SpawnComponents() {
		Debug.Log("Never implemented");
	}
}
