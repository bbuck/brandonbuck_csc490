using UnityEngine;
using System.Collections;

public class DamageText : MonoBehaviour {

	private float yAdjustment = 2;
	
	private int _damage;
	public int Damage {
		get {
			return _damage;
		}
		set {
			_damage = value;
			SetDamageText();
		}
	}
	
	private float startY;
	private float destinationY;
	private float step = 0;
	
	void Start() {
		startY = transform.position.y;
		destinationY = transform.position.y + yAdjustment;
	}
	
	void Update() {
		Vector3 position = transform.position;
		step += Time.deltaTime;
		position.y = Mathf.Lerp(startY, destinationY, step);
		
		transform.position = position;
		if (position.y >= destinationY) {
			Destroy(gameObject);
		}
	}
	
	#region helper functions
	
	private void SetDamageText() {
		Color textColor = (Damage > 0 ? Color.red : (Damage < 0 ? Color.green : Color.white));
		TextMesh textMesh = transform.Find("text").GetComponent<TextMesh>();
		textMesh.renderer.material.color = textColor;
		string damageString = string.Empty;
		if (Damage < 0)
			damageString = string.Format("+{0}", Mathf.Abs(Damage));
		else
			damageString = Damage.ToString();
		textMesh.text = damageString;
	}
	
	#endregion helper functions
}
