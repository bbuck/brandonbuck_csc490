using UnityEngine;
using System.Collections;

public class DoubleScorePowerup : BasicPowerup {
	protected override void ApplyEffect() {
		GameManager.Instance.SetDoubleScore();
	}
}
