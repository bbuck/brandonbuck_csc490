using UnityEngine;
using System.Collections;

public class FinalSpellPowerup : BasicPowerup {
	void Awake() {
		notification = "You unlocked METEOR!";	
	}
	
	protected override void ApplyEffect() {
		Player.Instance.UnlockFinalSpell();
	}
}
