using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	
	public static GameManager Instance {get; private set;}
	
	public int pointModifier = 10;
	public float doubleScoreLength = 2.5f;
	public float notificationTimeout = 5f;
	public int spawnPowerupPercentage = 15;
	
	public TextMesh scoreValueMesh;
	public TextMesh highScoreValueMesh;
	public TextMesh spellNameMesh;
	public TextMesh notificationMesh;
	
	// game objects for generating
	public GameObject damageTextObject;
	public GameObject ballObject;
	public GameObject rayBallObject;
	public GameObject explosionObject;
	public GameObject laserObject;
	public GameObject meteorBallObject;
	public GameObject meteorObject;
	
	// power up objects
	public GameObject manaUpPowerup;
	public GameObject manaDownPowerup;
	public GameObject splitBallPowerup;
	public GameObject healthRestorePowerup;
	public GameObject doubleScorePowerup;
	public GameObject addFinalPowerup;
	
	private int highScore = 0;
	private bool useRayball = false;
	private string[] code = {"UpArrow", "UpArrow", "DownArrow", "DownArrow", "LeftArrow", "RightArrow", "LeftArrow", "RightArrow", "B", "A", "Return"};
	private int codeStep = 0;
	private bool gameStarted = false;
	private int scoreMultiplier = 1;
	
	void Awake() {
		Instance = this;
		highScore = int.Parse(PlayerPrefs.GetString("breakoutRpg_highScore", "1337"));
		highScoreValueMesh.text = highScore.ToString();
	}
	
	void Update() {
		CheckBlockCount();
		CheckBallCount();
	}
	
	void OnGUI() {
		if (gameStarted || useRayball) return;
		Event evt = Event.current;
		if (evt.isKey && Input.anyKeyDown && evt.keyCode.ToString() != "None")
			TestInput(evt.keyCode);
	}
	
	#region public functions
	
	public void AddScore(int points) {
		int pointValue = points * pointModifier * scoreMultiplier;
		int currentScore = int.Parse(scoreValueMesh.text) + pointValue;
		scoreValueMesh.text = currentScore.ToString();
	}
	
	public GameObject CreateDamageText(int amount, Vector3 position) {
		if (amount > 0)
			AddScore(amount);
		GameObject damageText = CreateObject(damageTextObject, position);
		damageText.GetComponent<DamageText>().Damage = amount;
		return damageText;
	}
	
	public GameObject CreateBall(Vector3 position) {
		GameObject obj = (useRayball ? rayBallObject : ballObject);
		return CreateObject(obj, position);	
	}
	
	public GameObject CreateExplosion(Vector3 position) {
		return CreateObject(explosionObject, position);
	}
	
	public GameObject CreateLaser(Vector3 position) {
		return CreateObject(laserObject, position);	
	}
	
	public GameObject CreateMeteorBall(Vector3 position) {
		return CreateObject(meteorBallObject, position);	
	}
	
	public GameObject CreateMeteor(Vector3 position) {
		return CreateObject(meteorObject, position);	
	}
	
	public void Notify(string text) {
		if (text != null && text.Length == 0)
			return;
		notificationMesh.text = text;
		StartCoroutine("ClearNotification");
	}
	
	public void SpawnPowerup(Vector3 position) {
		// Determine if we spawn one
		int rand = Random.Range(1, 100);
		if (rand <= spawnPowerupPercentage) {
			GameObject powerup;
			// determine what powerup
			rand = Random.Range(1, 100);
			if (rand <= 45) {
				if (!Player.Instance.HasFinalSpell && rand <= 10)
					powerup = addFinalPowerup;
				else
					powerup = manaDownPowerup;
			}
			else if (rand <= 65)
				powerup = manaUpPowerup;
			else if (rand <= 85)
				powerup = splitBallPowerup;
			else if (rand <= 95)
				powerup = doubleScorePowerup;
			else
				powerup = healthRestorePowerup;
			CreateObject(powerup, position);
		}
	}
	
	public void SetSpellName(string name) {
		spellNameMesh.text = name;
	}
	
	public void SetSpellInactive(bool active) {
		if (active)
			spellNameMesh.renderer.material.color = Color.white;
		else
			spellNameMesh.renderer.material.color = new Color(0.3f, 0.3f, 0.3f);
	}
	
	public void SetDoubleScore() {
		scoreMultiplier = 2;
		StartCoroutine("ResetScoreMultiplier");
	}
	
	public void GameStarted() {
		gameStarted = true;
	}
	
	#endregion public functions
	
	#region helper functions
	
	void CheckBallCount() {
		if (Player.Instance.HasLaunchedBall) {
			int ballCount = GameObject.FindGameObjectsWithTag("ball").Length;
			if (ballCount == 0 && Player.Instance.HP.CurrentHealth > 0)	{
				Player.Instance.HP.Damage();
				Player.Instance.ResetState();
			}
			
			if (Player.Instance.HP.CurrentHealth <= 0)	
				ReloadLevel();
		}
	}
	
	GameObject CreateObject(GameObject obj, Vector3 pos) {
		return CreateObject(obj, pos, Quaternion.identity);
	}
	
	GameObject CreateObject(GameObject obj, Vector3 pos, Quaternion rot) {
		return Instantiate(obj, pos, rot) as GameObject;
	}
	
	void CheckBlockCount() {
		if (GameObject.FindGameObjectsWithTag("block").Length == 0) {
			ReloadLevel();
		}
	}
	
	void ReloadLevel() {
		int currentScore = int.Parse(scoreValueMesh.text);
		if (currentScore > highScore)
			PlayerPrefs.SetString("breakoutRpg_highScore", currentScore.ToString());
		Application.LoadLevel(Application.loadedLevel);	
	}
	
	void TestInput(KeyCode keyCode) {
		print(keyCode.ToString());
		if (code[codeStep] == keyCode.ToString()) {
			codeStep++;
			if (codeStep >= code.Length) {
				CreateExplosion(new Vector3(0, 0, 2));
				useRayball = true;
			}
		}
		else
			codeStep = 0;
	}
	
	IEnumerator ResetScoreMultiplier() {
		yield return new WaitForSeconds(doubleScoreLength);
		scoreMultiplier = 1;
		StopCoroutine("ResetScoreMultiplier");
	}
	
	IEnumerator ClearNotification() {
		yield return new WaitForSeconds(notificationTimeout);
		notificationMesh.text = string.Empty;
		StopCoroutine("ClearNotification");
	}
	
	#endregion helper functions
}
