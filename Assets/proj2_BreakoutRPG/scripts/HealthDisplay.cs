using UnityEngine;
using System.Collections;

public class HealthDisplay : MonoBehaviour {
	public HealthSystem healthSystem;
	
	void Awake() {
		healthSystem.HealthChanged += OnHealthChanged;
		GetComponent<TextMesh>().text = healthSystem.CurrentHealth.ToString();
	}
	
	#region helper functions
	
	void OnHealthChanged(int amount, int currentHealth) {
		GetComponent<TextMesh>().text = currentHealth.ToString();	
	}
	
	#endregion helper functions
}
