using UnityEngine;
using System.Collections;

public class HealthUpPowerup : BasicPowerup {
	protected override void ApplyEffect() {
		Player.Instance.HP.Heal();	
	}
}
