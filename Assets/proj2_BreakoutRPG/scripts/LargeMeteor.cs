using UnityEngine;
using System.Collections;

public class LargeMeteor : Ball {
	
	public int scoreValue = 50;
	
	void OnTriggerEnter(Collider collider) {
		if (collider.gameObject.CompareTag("block")) {
			collider.gameObject.GetComponent<BasicBlock>().Remove();
			GameManager.Instance.AddScore(scoreValue);
		}
	}
	
	#region helper functions
	
	protected override void CheckBounds() {
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		
		if (bottomLeft.y > 1)
			Destroy(gameObject);
	}
	
	protected override void HandleDeadZoneCollision() {
		// Do nothing	
	}
	
	#endregion helper functions
}
