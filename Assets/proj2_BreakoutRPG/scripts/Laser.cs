using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {
	
	public float speed = 15;
	public int damage = 6;
	
	// Use this for initialization
	void Start() {
		rigidbody.velocity = Vector3.up * speed;
	}
	
	void Update() {
		Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
		if (screenPoint.y > 1) {
			Destroy(gameObject);
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.CompareTag("block")) {
			collision.gameObject.GetComponent<BasicBlock>().Damage(damage);
			Destroy(gameObject);
		}
	}
}
