using UnityEngine;
using System.Collections;

public class LaserSpell : BasicSpell {
	private float xAdjustment = 0.8f;
	
	public LaserSpell() : base("Laser", 7.5f, 40) { }
	
	protected override void SpawnComponents() {
		Vector3 playerPos = Player.Instance.transform.position;
		Vector3 playerExtents = Player.Instance.collider.bounds.extents;
		
		float maxX = playerPos.x + playerExtents.x + xAdjustment;
		float minX = playerPos.x - playerExtents.x - xAdjustment;
		
		GameManager.Instance.CreateLaser(new Vector3(maxX, playerPos.y, 1));
		GameManager.Instance.CreateLaser(new Vector3(minX, playerPos.y, 1));
	}
}
