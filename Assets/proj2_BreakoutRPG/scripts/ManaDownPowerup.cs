using UnityEngine;
using System.Collections;

public class ManaDownPowerup : BasicPowerup {
	
	public float manaDestroyValue = 25;
	
	protected override void ApplyEffect() {
		Player.Instance.Mana.Remove(manaDestroyValue);	
	}
}
