using UnityEngine;
using System.Collections;

public class ManaSystem : MonoBehaviour {
	
	public float maxMana = 100;
	public float manaPerSecond = 5;
	
	public float CurrentMana {get; private set;}
	
	void Awake() {
		CurrentMana = maxMana;	
	}
	
	void Update() {
		RegenMana();
	}
	
	#region public functions
	
	public bool HasMana(float amount) {
		return CurrentMana > amount;	
	}
	
	public bool Remove(float amount) {
		return Remove(amount, false);
	}
	
	public bool Remove(float amount, bool force) {
		if (force || HasMana(amount)) {
			CurrentMana -= amount;
			ClampMana();
			return true;
		}
		else
			return false;
	}
	
	public void Restore(float amount) {
		if (amount <= 0)
			return;
		CurrentMana += amount;
		ClampMana();
	}
	
	#endregion public functions
	
	#region helper functions
	
	void RegenMana() {
		CurrentMana += manaPerSecond * Time.deltaTime;
		ClampMana();
	}
	
	void ClampMana() {
		if (CurrentMana > maxMana)
			CurrentMana = maxMana;
		else if (CurrentMana < 0)
			CurrentMana = 0;
	}
	
	#endregion helper functions
}
