using UnityEngine;
using System.Collections;

public class ManaUpPowerup : BasicPowerup {
	
	public float manaRestoreValue = 50;
	
	protected override void ApplyEffect() {
		Player.Instance.Mana.Restore(manaRestoreValue);	
	}
}
