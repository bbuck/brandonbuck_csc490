using UnityEngine;
using System.Collections;

public class MeteorBall : Ball {
	void OnCollisionEnter(Collision collision) {
		base.HandleCollision(collision);
		if (collision.gameObject.CompareTag("block")) {
			Destroy(gameObject);
		}
	}
}
