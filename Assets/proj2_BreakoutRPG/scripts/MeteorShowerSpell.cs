using UnityEngine;
using System.Collections;

public class MeteorShowerSpell : BasicSpell {
	private int meteorCount = 5;
	
	public MeteorShowerSpell() : base("Meteor Shower", 5.0f, 50) { }

	protected override void SpawnComponents() {
		for (int i = 0; i < meteorCount; i++) {
			GameManager.Instance.CreateMeteorBall(Player.Instance.BallSpawn);
		}
	}
}