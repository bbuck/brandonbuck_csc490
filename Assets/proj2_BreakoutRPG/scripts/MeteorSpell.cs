using UnityEngine;
using System.Collections;

public class MeteorSpell : BasicSpell {
	public MeteorSpell() : base("Meteor", 10.0f, 80) { }
	
	protected override void SpawnComponents() {
		GameManager.Instance.CreateMeteor(Player.Instance.BallSpawn);
	}
}
