using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	public static Player Instance {get; private set;}
	
	public float speed = 10;
	
	private BasicSpell[] spells = {new LaserSpell(), new MeteorShowerSpell(), new MeteorSpell()};
	private int spellCount = 2; // Meteor has to be unlocked
	private int selectedSpell = 0;
	
	private float widthPerMana;
	private float halfWidth;
	
	public bool HasFinalSpell {get; private set;}
	public bool HasLaunchedBall {get; private set;}
	
	public ManaSystem Mana {get; private set;}
	public HealthSystem HP {get; private set;}
	
	public Vector3 BallSpawn {
		get {
			return transform.Find("ball_spawn").position;		
		}
	}
	
	private Transform DummyBall {
		get {
			return transform.Find("ball_spawn/dummy_ball");
		}
	}
	
	private BasicSpell CurrentSpell {
		get {
			if (selectedSpell < 0)
				selectedSpell = spellCount - 1;
			else if (selectedSpell >= spellCount)
				selectedSpell = 0;
			return spells[selectedSpell];		
		}
	}
	
	void Awake() {
		Instance = this;
		Mana = gameObject.GetComponent<ManaSystem>();
		HP = gameObject.GetComponent<HealthSystem>();
		HasLaunchedBall = false;
		halfWidth = transform.localScale.x / 2;
		widthPerMana = halfWidth / Mana.maxMana;
	}
	
	// Update is called once per frame
	void Update() {
		ProcessMovement();
		CheckBounds();
		AdjustScaleToMana();
	}
	
	#region public functions
	
	public void UnlockFinalSpell() {
		HasFinalSpell = true;
		spellCount++;
	}
	
	public void ResetState() {
		HasLaunchedBall = false;
		DummyBall.renderer.enabled = true;
	}
	
	#endregion public functions
	
	#region Helper functions
	
	void ProcessMovement() {
		rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed;
		
		if (!HasLaunchedBall && Input.GetAxis("Launch") > 0) {
			DummyBall.renderer.enabled = false;
			Vector3 spawnPos = BallSpawn;
			Vector3 location = new Vector3(spawnPos.x, spawnPos.y, 1);
			GameManager.Instance.CreateBall(location);
			GameManager.Instance.GameStarted();
			HasLaunchedBall = true;
		}
		
		if (Input.GetButtonDown("Previous Spell"))
			selectedSpell--;
		if (Input.GetButtonDown("Next Spell"))
			selectedSpell++;
		
		bool spellIsCastable = HasLaunchedBall && CurrentSpell.IsCastable(Time.time, Mana.CurrentMana);
		
		GameManager.Instance.SetSpellInactive(spellIsCastable);
		GameManager.Instance.SetSpellName(CurrentSpell.Name);
		
		if (Input.GetButtonDown("Cast") && spellIsCastable && HasLaunchedBall)
			CurrentSpell.Cast(Time.time);
	}
	
	void CheckBounds() {
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		
		if (topRight.x > 1 && rigidbody.velocity.x > 0)
			rigidbody.velocity = Vector3.zero;
		else if (bottomLeft.x < 0 && rigidbody.velocity.x < 0)
			rigidbody.velocity = Vector3.zero;
	}
	
	void AdjustScaleToMana() {
		Vector3 scale = transform.localScale;
		scale.x = halfWidth + (Mana.CurrentMana * widthPerMana);
		Transform dummyBall = DummyBall;
		Transform dummyBallParent = dummyBall.parent;
		dummyBall.parent = null;
		transform.localScale = scale;
		dummyBall.parent = dummyBallParent;
	}
	
	#endregion Helper functions
}