using UnityEngine;
using System.Collections;

public class SplitBallPowerup : BasicPowerup {
	public int maxNumberOfConcurrentBalls = 6;
	
	protected override void ApplyEffect() {
		GameObject[] balls = GameObject.FindGameObjectsWithTag("ball");
		if (balls.Length < maxNumberOfConcurrentBalls) {
			foreach (GameObject go in balls) {
				Vector3 position = go.transform.position;
				Vector3 velocity = go.rigidbody.velocity;
				velocity *= -1;
				GameObject newBall = GameManager.Instance.CreateBall(position);
				newBall.rigidbody.velocity = velocity;
			}
		}
	}
}
