using UnityEngine;
using System.Collections;

public class Warden : MonoBehaviour {
	
	public int numberOfBlocksToHeal = 25;
	public float healDelayTime = 25f;
	
	void Start() {
		StartCoroutine(HealBlocks());	
	}
	
	#region helper methods
	
	IEnumerator HealBlocks() {
		while (true) {
			yield return new WaitForSeconds(healDelayTime);
			GameObject[] blocks = GameObject.FindGameObjectsWithTag("block");
			if (blocks.Length < numberOfBlocksToHeal) {
				foreach (GameObject block in blocks) {
					HealBlock(block);
				}
			}
			else {
				int maxValue = blocks.Length - numberOfBlocksToHeal + 1;
				int start = Random.Range(0, maxValue);
				for (int i = start; i < (start + numberOfBlocksToHeal); i++) {
					HealBlock(blocks[i]);
				}
			}
		}
	}
	
	void HealBlock(GameObject block) {
		int healValue = Random.Range(1, 3);
		block.GetComponent<BasicBlock>().Damage(-healValue);
	}
	
	#endregion helper methods
}
