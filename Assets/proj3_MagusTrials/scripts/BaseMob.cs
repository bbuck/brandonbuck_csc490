using UnityEngine;
using System.Collections;

namespace MagusTrials
{
    [RequireComponent(typeof(HealthSystem))]
    public class BaseMob : MonoBehaviour
    {
        public float speed = 1f;
        public MagicType mobType;

        protected HealthSystem health;

        void OnEnable()
        {
            Enable();
        }

        void OnDisable()
        {
            Disable();
        }

        void Update()
        {
            OnUpdate();
        }

        #region helper functions

        protected virtual void Enable()
        {
            health = GetComponent<HealthSystem>();
            health.HealthChanged += OnDamageTaken;
        }

        protected virtual void Disable() 
        {
            health = GetComponent<HealthSystem>();
            health.HealthChanged -= OnDamageTaken;
        }

        protected virtual void OnUpdate() { }

        protected virtual void OnDamageTaken(int amount, int currentHealth)
        {
            if (currentHealth <= 0)
                Destroy(gameObject);
        }

        #endregion helper functions

        #region public functions

        public virtual bool IsWeakTo(MagicType type)
        {
            // FrostFire is only weak to FrostFire
            if (mobType == MagicType.FrostFire)
                return type == MagicType.FrostFire;

            if (type == MagicType.FrostFire)
                return true; // Always weak
            else
                return type != mobType;
        }

        #endregion public functions
    }
}
