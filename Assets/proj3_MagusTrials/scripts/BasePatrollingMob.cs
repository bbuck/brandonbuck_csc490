using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    [RequireComponent(typeof(Sprite))]
    [RequireComponent(typeof(SquarePlane))]
    [RequireComponent(typeof(CharacterMotor))]
    public class BasePatrollingMob : BaseMob
    {
        public float range = 10f;
        public bool direction = false; // false is left

        protected Sprite sprite;
        protected CharacterMotor motor;
        protected SquarePlane plane;
        protected float maxX, minX;

        void OnCollisionEnter(Collision collision)
        {
            ChangeDirection();
        }

        protected override void Enable()
        {
            base.Enable();
            motor = GetComponent<CharacterMotor>();
            sprite = GetComponent<Sprite>();
            plane = GetComponent<SquarePlane>();
            sprite.FrameChanged += OnFrameChanged;
            Vector3 pos = transform.position;
            maxX = pos.x + range;
            minX = pos.x - range;
        }

        protected override void Disable()
        {
            sprite.FrameChanged -= OnFrameChanged;
        }

        protected override void OnUpdate()
        {
            if (direction)
            {
                motor.targetVelocity.x = speed;
                if (transform.position.x > maxX)
                    ChangeDirection();
            }
            else
            {
                motor.targetVelocity.x = -speed;
                if (transform.position.x < minX)
                    ChangeDirection();
            }
        }

        protected virtual void ChangeDirection()
        {
            direction = !direction;
            plane.UVCoords.Flip = direction;
        }

        protected virtual void OnFrameChanged(string frameName) { }
    }
}