using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class BasePatrollingMobWithVision : BasePatrollingMob
    {
        protected GameObject vision;
        protected bool canSeePlayer = false;

        protected override void Enable()
        {
            base.Enable();
            vision = transform.Find("vision").gameObject;
            MobVision mobVision = vision.GetComponent<MobVision>();
            mobVision.PlayerSighted += OnPlayerSighted;
            mobVision.LostPlayer += OnLostPlayer;
        }

        protected override void Disable()
        {
            base.Disable();
            MobVision mobVision = vision.GetComponent<MobVision>();
            mobVision.PlayerSighted -= OnPlayerSighted;
            mobVision.LostPlayer -= OnLostPlayer;
        }

        protected override void ChangeDirection()
        {
            base.ChangeDirection();
            Vector3 oldVisionPoint = vision.transform.localPosition;
            oldVisionPoint.x = direction ? Mathf.Abs(oldVisionPoint.x) : -Mathf.Abs(oldVisionPoint.x);
            vision.transform.localPosition = oldVisionPoint;
        }

        protected virtual void OnPlayerSighted(Collider player) 
        {
            canSeePlayer = true;
        }

        protected virtual void OnLostPlayer(Collider collider) 
        {
            canSeePlayer = false;
        }
    }
}