using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    [RequireComponent(typeof(Sprite))]
    [RequireComponent(typeof(SquarePlane))]
    public class BaseSpell : MonoBehaviour
    {
        public float initialVelocity = 5f;
        public List<string> doesNotCollideWithTags = new List<string>();
        public int damage = 1;
        public MagicType magicType;
        public bool handleAnimationChanges = true;
        public bool destroyWhenOffscreen = true;

        protected bool fired = false;
        protected Sprite sprite;
        protected SquarePlane plane;

        void OnEnable()
        {
            sprite = GetComponent<Sprite>();
            if (handleAnimationChanges)
                sprite.AnimationPlaying = "Cast";
            plane = GetComponent<SquarePlane>();
        }

        void OnTriggerEnter(Collider collider)
        {
            string tag = collider.gameObject.tag;
            if (tag == null)
                tag = "No Tag";
            if (!doesNotCollideWithTags.Contains(collider.gameObject.tag))
            {
                if (handleAnimationChanges)
                    sprite.AnimationPlaying = "Hit";
                Hit(collider);
            }
        }

        void Update()
        {
            if (destroyWhenOffscreen)
            {
                Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
                Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
                if (bottomLeft.x > 1f || topRight.x < 0f)
                    Destroy(gameObject);
            }
        }

        #region helper functions

        protected virtual void Hit(Collider collider) { }

        protected void DamageMob(GameObject mobGameObject)
        {
            bool mobIsWeak = true; // If not a mob, assume it's weak
            BaseMob mob = mobGameObject.GetComponent<BaseMob>();
            if (mob != null)
                mobIsWeak = mob.IsWeakTo(magicType);

            HealthSystem health = mobGameObject.GetComponent<HealthSystem>();
            if (health != null)
            {
                if (mobIsWeak)
                    health.Damage(damage);
                else
                    health.Heal(damage);
            }
        }

        void ShootForward()
        {
            Vector3 fireDirection = (plane.UVCoords.Flip ? Vector3.left : Vector3.right);
            fireDirection.x *= initialVelocity;
            rigidbody.AddForce(fireDirection, ForceMode.VelocityChange);
            if (handleAnimationChanges)
                sprite.AnimationPlaying = "Flying";
        }

        #endregion helper functions

        #region public functions

        public void Fire()
        {
            if (fired)
                return;
            fired = true;
            Invoke("ShootForward", .01f);
        }

        public void Flip()
        {
            plane.UVCoords.Flip = !plane.UVCoords.Flip;
            plane.GenerateMesh();
        }

        public void Flip(bool flip)
        {
            plane.UVCoords.Flip = flip;
            plane.GenerateMesh();
        }

        #endregion public functions
    }
}
