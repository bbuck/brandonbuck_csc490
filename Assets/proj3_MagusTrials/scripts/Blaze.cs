using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class Blaze : BasePatrollingMobWithVision
    {
        public float chargeCooldown = 2f;
        public float chargeForce = 20f;

        private float nextCharge = 0f;

        protected override void Enable()
        {
            base.Enable();
            sprite.AnimationPlaying = "Floating";
        }

        protected override void OnUpdate()
        {
 	        base.OnUpdate();
            if (canSeePlayer && Time.time > nextCharge)
                Charge();
        }

        protected override void ChangeDirection()
        {
            if (canSeePlayer)
                return;
            base.ChangeDirection();
        }

        protected override void OnPlayerSighted(Collider player)
        {
            base.OnPlayerSighted(player);
            Charge();
        }

        protected override void OnLostPlayer(Collider collider)
        {
            base.OnLostPlayer(collider);
            nextCharge = 0f;
        }

        void Charge()
        {
            rigidbody.AddForce((direction ? Vector3.right : Vector3.left) * chargeForce, ForceMode.Impulse);
            nextCharge = Time.time + chargeCooldown;
        }
    }
}