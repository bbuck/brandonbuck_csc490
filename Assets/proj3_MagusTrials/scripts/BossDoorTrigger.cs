using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class BossDoorTrigger : MonoBehaviour
    {
        public GameObject bossDoor;

        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("magus_player"))
            {
                bossDoor.GetComponent<Animation>().Play("bossDoorClosing");
                Destroy(gameObject);
            }
        }
    }
}