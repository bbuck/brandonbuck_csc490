using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class BossFireball : BaseSpell
    {
        protected override void Hit(Collider collider)
        {
            DamageMob(collider.gameObject);
            Destroy(gameObject);
        }
    }
}