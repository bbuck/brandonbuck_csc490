using UnityEngine;
using System;
using System.Collections;

namespace MagusTrials
{
    public class FakePlatform : MonoBehaviour
    {
        public float beforeDissapearDuration = 0.2f;
        public float respawnDuration = 3f;
        
        private float hiddenZ = -20;
        private float platformZ = 0f;

        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("magus_player"))
                Invoke("Hide", beforeDissapearDuration);
        }

        #region helper functions

        void Hide()
        {
            Vector3 position = transform.position;
            position.z = hiddenZ;
            transform.position = position;
            Invoke("Show", respawnDuration);
        }

        void Show()
        {
            Vector3 position = transform.position;
            position.z = platformZ;
            transform.position = position;
        }

        #endregion helper functions
    }
}
