using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class FinalPortal : MonoBehaviour
    {
        public static FinalPortal Instance { get; private set; }

        private bool _fireLordDead = false;
        public bool FireLordDead
        {
            get { return _fireLordDead; }
            set
            {
                _fireLordDead = value;
                if (_fireLordDead)
                    ShowPortal();
            }
        }

        private bool _iceLordDead = false;
        public bool IceLordDead
        {
            get { return _iceLordDead; }
            set
            {
                _iceLordDead = value;
                if (_iceLordDead)
                    ShowPortal();
            }
        }

        void Start()
        {
            Instance = this;
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("magus_player"))
            {
                Application.LoadLevel("GameOver");
            }
        }

        void ShowPortal()
        {
            if (IceLordDead && FireLordDead)
            {
                Vector3 pos = transform.position;
                pos.z = 0;
                transform.position = pos;
            }
        }
    }
}