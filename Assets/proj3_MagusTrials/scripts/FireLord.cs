using UnityEngine;
using UnityRandom = UnityEngine.Random;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    enum FireLordPhases
    {
        Deciding,
        MovingRight,
        MovingLeft,
        MovingTop,
        MovingBottom,
        Firing
    }

    [RequireComponent(typeof(SquarePlane))]
    [RequireComponent(typeof(CharacterMotor))]
    public class FireLord : BaseMob
    {
        public Transform leftBound, rightBound, topBound, bottomBound, bossFireballLaunch;
        public float fireballCooldown = .8f;
        public bool flipped = false;

        private FireLordPhases currentPhase = FireLordPhases.Deciding;
        private bool hasBeenInitiated = false;
        private float nextCast = 0f;
        private SquarePlane MeshPlane { get; set; }
        private CharacterMotor Motor { get; set; }

        protected override void Enable()
        {
            base.Enable();
            MeshPlane = GetComponent<SquarePlane>();
            MeshPlane.UVCoords.Flip = flipped;
            Motor = GetComponent<CharacterMotor>();
        }

        protected override void OnUpdate()
        {
            if (hasBeenInitiated)
            {
                Vector3 currentPos = transform.position;
                Motor.targetVelocity = Vector3.zero;
                switch (currentPhase)
                {
                    case FireLordPhases.Deciding:
                        DecideNextPhase();
                        break;
                    case FireLordPhases.MovingRight:
                        if (currentPos.y < topBound.position.y)
                            Motor.targetVelocity = Vector3.up * speed;
                        else
                        {
                            if (currentPos.x < rightBound.position.x)
                                Motor.targetVelocity = Vector3.right * speed;
                            else
                            {
                                Flip();
                                currentPhase = FireLordPhases.Deciding;
                            }
                        }
                        break;
                    case FireLordPhases.MovingLeft:
                        if (currentPos.y < topBound.position.y)
                            Motor.targetVelocity = Vector3.up * speed;
                        else
                        {
                            if (currentPos.x > leftBound.position.x)
                                Motor.targetVelocity = Vector3.left * speed;
                            else
                            {
                                Flip();
                                currentPhase = FireLordPhases.Deciding;
                            }
                        }
                        break;
                    case FireLordPhases.MovingTop:
                        if (currentPos.y < topBound.position.y)
                            Motor.targetVelocity = Vector3.up * speed;
                        else
                            currentPhase = FireLordPhases.Deciding;
                        break;
                    case FireLordPhases.MovingBottom:
                        if (currentPos.y > bottomBound.position.y)
                            Motor.targetVelocity = Vector3.down * speed;
                        else
                            currentPhase = FireLordPhases.Deciding;
                        break;
                    case FireLordPhases.Firing:
                        Fire();
                        currentPhase = FireLordPhases.Deciding;
                        break;
                }
            }
        }

        #region helper functions

        protected override void OnDamageTaken(int amount, int currentHealth)
        {
            if (currentHealth <= 0)
            {
                FinalPortal.Instance.FireLordDead = true;
                Destroy(gameObject);
            }
        }

        FireLordPhases GetMoveUpDown()
        {
            Vector3 pos = transform.position;
            float distanceTop = Mathf.Abs(pos.y - topBound.position.y);
            float distanceBottom = Mathf.Abs(pos.y - bottomBound.position.y);
            if (distanceTop > distanceBottom)
                return FireLordPhases.MovingTop;
            else
                return FireLordPhases.MovingBottom;
        }

        FireLordPhases GetMoveLeftRight()
        {
            Vector3 pos = transform.position;
            float distanceLeft = Mathf.Abs(pos.x - leftBound.position.x);
            float distanceRight = Mathf.Abs(pos.x - rightBound.position.x);
            if (distanceLeft > distanceRight)
                return FireLordPhases.MovingLeft;
            else
                return FireLordPhases.MovingRight;
        }

        void DecideNextPhase()
        {
            int decision = UnityRandom.Range(1, 100);
            if (decision < 30)
                currentPhase = GetMoveUpDown();
            else if (decision < 40)
                currentPhase = GetMoveLeftRight();
            else
            {
                if (Time.time > nextCast)
                    currentPhase = FireLordPhases.Firing;
                else
                    currentPhase = GetMoveUpDown();
            }
        }

        void Flip()
        {
            flipped = !flipped;
            MeshPlane.UVCoords.Flip = flipped;
            Vector3 localPos = bossFireballLaunch.localPosition;
            if (flipped)
                localPos.x = Mathf.Abs(localPos.x);
            else
                localPos.x = -Mathf.Abs(localPos.x);
            bossFireballLaunch.localPosition = localPos;
            MeshPlane.GenerateMesh();
        }

        void Fire()
        {
            GameObject bossFireball = PrefabManager.Instance.CreateInstance("Boss Fireball", bossFireballLaunch.position);
            BossFireball bFball = bossFireball.GetComponent<BossFireball>();
            bFball.Flip(!flipped);
            bFball.Fire();
            nextCast = Time.time + fireballCooldown;
        }

        #endregion helper functions

        #region public functions

        public void Initiate()
        {
            hasBeenInitiated = true;
        }

        #endregion public functions
    }
}