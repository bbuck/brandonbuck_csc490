using UnityEngine;
using System;
using System.Collections;

namespace MagusTrials
{
    public class Fireball : BaseSpell
    {
        protected override void Hit(Collider collider)
        {
            bool flipped = plane.UVCoords.Flip;
            Vector3 pos = transform.position;
            if (flipped)
                pos.x += 0.24f;
            else
                pos.x -= 0.24f;
            transform.position = pos;
            Destroy(gameObject, 0.2f);
            rigidbody.velocity = Vector3.zero;
            DamageMob(collider.gameObject);
        }
    }
}
