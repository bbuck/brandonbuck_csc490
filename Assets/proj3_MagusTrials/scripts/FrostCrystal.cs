using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class FrostCrystal : MonoBehaviour
    {
        public GameObject fontPrefab;
        public GameObject fontLocation;

        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("magus_player"))
            {
                collider.gameObject.GetComponent<PlayerController>().canCastFrost = true;
                Destroy(gameObject.transform.parent.gameObject);
                Instantiate(fontPrefab, fontLocation.transform.position, Quaternion.identity);
            }
        }
    }
}