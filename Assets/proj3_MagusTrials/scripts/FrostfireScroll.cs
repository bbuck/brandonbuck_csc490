using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class FrostfireScroll : MonoBehaviour
    {
        public Transform platform;

        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("magus_player"))
            {
                collider.gameObject.GetComponent<PlayerController>().canCastFrostFire = true;
                Destroy(gameObject.transform.parent.gameObject);
                Vector3 pos = platform.position;
                pos.z = 0;
                platform.position = pos;
            }
        }
    }
}