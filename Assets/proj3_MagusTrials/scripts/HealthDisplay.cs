using UnityEngine;
using System;
using System.Collections;

namespace MagusTrials
{
    public class HealthDisplay : MonoBehaviour
    {
        public GameObject lifeOne, lifeTwo, lifeThree;

        private bool registered = false;

        void OnEnable()
        {
            if (PlayerController.Instance != null)
                RegisterEventListener();
        }

        void Update()
        {
            if (!registered && PlayerController.Instance != null)
                RegisterEventListener();
        }

        void OnDisable()
        {
            PlayerController.Instance.Health.HealthChanged -= OnHealthChanged;
        }

        void RegisterEventListener()
        {
            registered = true;
            PlayerController.Instance.Health.HealthChanged += OnHealthChanged;
        }

        void OnHealthChanged(int amount, int currentHealth)
        {
            bool showOne, showTwo, showThree;
            showOne = showTwo = showThree = false;
            if (currentHealth >= 1)
                showOne = true;
            if (currentHealth >= 2)
                showTwo = true;
            if (currentHealth >= 3)
                showThree = true;

            lifeOne.renderer.enabled = showOne;
            lifeTwo.renderer.enabled = showTwo;
            lifeThree.renderer.enabled = showThree;

            if (currentHealth <= 0)
            {
                Application.LoadLevel("GameOver");
            }
        }
    }
}
