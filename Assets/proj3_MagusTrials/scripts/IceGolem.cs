using UnityEngine;
using System.Collections;

namespace MagusTrials
{
    public class IceGolem : BasePatrollingMobWithVision
    {
        public float attackForce = 10f;
        public float punchCooldown = 2f;

        private float nextPunch = 0f;

        #region helper functions

        protected override void OnUpdate()
        {
            base.OnUpdate();
            if (canSeePlayer && Time.time > nextPunch)
                sprite.AnimationPlaying = "Attacking";
        }

        protected override void Enable()
        {
            base.Enable();
            sprite.AnimationPlaying = "Walking";
        }

        protected override void ChangeDirection()
        {
            if (canSeePlayer)
                return;
            base.ChangeDirection();
        }

        protected override void OnFrameChanged(string frameName)
        {
            switch (frameName)
            {
                case "IceGolemAttacking1.png":
                    rigidbody.AddForce((direction ? Vector3.right : Vector3.left) * attackForce, ForceMode.Impulse);
                    break;
                case "IceGolemAttacking2.png":
                    sprite.AnimationPlaying = "Walking";
                    nextPunch = Time.time + punchCooldown;
                    break;
            }
        }

        protected override void OnPlayerSighted(Collider player)
        {
            base.OnPlayerSighted(player);
            sprite.AnimationPlaying = "Attacking";
        }

        protected override void OnLostPlayer(Collider player)
        {
            base.OnLostPlayer(player);
            nextPunch = 0f;
        }

        #endregion helper functions
    }
}
