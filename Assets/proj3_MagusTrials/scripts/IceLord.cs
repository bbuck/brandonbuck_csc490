using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    enum IceLordPhases
    {
        MovingUp,
        Targeting,
        Crushing
    }

    [RequireComponent(typeof(CharacterMotor))]
    public class IceLord : BaseMob
    {
        public float crushSpeed = 5f;
        public float crushDelay = 0.3f;
        public float restDelay = 0.5f;
        public Transform finalBossNecessities, fireLordSpawn;

        private HealthSystem Health { get; set; }
        private CharacterMotor Motor { get; set; }
        private bool hasBeenInitiated = false;
        private bool isInvulnerable = true;
        private IceLordPhases currentPhase = IceLordPhases.MovingUp;
        private float topOfScreenY = 0f;
        private bool hasSpawnedFirelord = false;

        protected override void Enable()
        {
            base.Enable();
            Health = GetComponent<HealthSystem>();
            Motor = GetComponent<CharacterMotor>();
        }

        protected override void OnUpdate()
        {
            if (hasBeenInitiated)
            {
                Vector3 currentPos = transform.position;
                float quaterWidth = renderer.bounds.size.x / 4f;
                Vector3 playerPos = PlayerController.Instance.transform.position;
                Motor.targetVelocity = Vector3.zero;
                switch (currentPhase)
                {
                    case IceLordPhases.MovingUp:
                        if (currentPos.y < topOfScreenY)
                            Motor.targetVelocity = Vector3.up * speed;
                        else
                            currentPhase = IceLordPhases.Targeting;
                        break;
                    case IceLordPhases.Crushing:
                        Invoke("Crush", crushDelay);
                        break;
                    case IceLordPhases.Targeting:
                        Vector3 direction = Vector3.zero;
                        if (playerPos.x > currentPos.x + quaterWidth)
                            direction = Vector3.right;
                        else if (playerPos.x < currentPos.x - quaterWidth)
                            direction = Vector3.left;
                        Motor.targetVelocity = direction * speed;
                        if (direction == Vector3.zero)
                            currentPhase = IceLordPhases.Crushing;
                        break;
                }
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("magus_player"))
                ResetPhase();
            else
                Invoke("ResetPhase", restDelay);
        }

        protected override void OnDamageTaken(int amount, int currentHealth)
        {
            if (isInvulnerable)
            {
                Health.SetCurrentHealth(currentHealth + Mathf.Abs(amount), true);
                if (!hasBeenInitiated)
                {
                    hasBeenInitiated = true;
                    Vector3 topOfScreen = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1f, 0f));
                    topOfScreenY = topOfScreen.y;
                    isInvulnerable = false;
                    ShowNecessities();
                }
            }
            else
            {
                if (!hasSpawnedFirelord && currentHealth <= (Health.maxHealth / 2))
                    SpawnFireLord();
                else if (currentHealth <= 0)
                {
                    FinalPortal.Instance.IceLordDead = true;
                    Destroy(gameObject);
                }
            }
        }

        void ResetPhase()
        {
            currentPhase = IceLordPhases.MovingUp;
        }

        void Crush()
        {
            rigidbody.AddForce(Vector3.down * crushSpeed, ForceMode.Impulse);
        }

        void ShowNecessities()
        {
            Vector3 pos = finalBossNecessities.position;
            pos.z = 0;
            finalBossNecessities.position = pos;
        }

        void SpawnFireLord()
        {
            hasSpawnedFirelord = true;
            GameObject fireLord = PrefabManager.Instance.CreateInstance("Fire Lord", fireLordSpawn.position);
            FireLord fLord = fireLord.GetComponent<FireLord>();
            Transform finalBoss = transform.parent;
            fLord.leftBound = finalBoss.Find("left_bound");
            fLord.rightBound = finalBoss.Find("right_bound");
            fLord.topBound = finalBoss.Find("top_bound");
            fLord.bottomBound = finalBoss.Find("bottom_bound");
            fLord.Initiate();
            Health.SetCurrentHealth(Health.maxHealth, true);
        }
    }
}