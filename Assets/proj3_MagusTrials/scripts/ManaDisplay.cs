using UnityEngine;
using System.Collections;

namespace MagusTrials
{
    public class ManaDisplay : MonoBehaviour
    {
        public GameObject fireManaBar;
        public GameObject frostManaBar;

        private float fireMaxWidth;
        private float fireIncr = 0f;
        private float frostMaxWidth;
        private float frostIncr = 0f;
        private bool registered = false;

        private bool canCastFire = false;
        private bool canCastFrost = false;

        void OnEnable()
        {
            fireMaxWidth = fireManaBar.transform.localScale.x;
            frostMaxWidth = frostManaBar.transform.localScale.x;
            if (PlayerController.Instance != null)
                RegisterEventListener();
            fireManaBar.renderer.enabled = false;
            frostManaBar.renderer.enabled = false;
        }

        void OnDisable()
        {
            PlayerController.Instance.Mana.ManaChanged -= OnManaChanged;
        }

        void Update()
        {
            if (PlayerController.Instance != null)
            {
                if (!registered)
                    RegisterEventListener();

                PlayerController player = PlayerController.Instance;
                if (!canCastFire)
                {
                    canCastFire = player.canCastFire;
                    bool canCastFrostFire = player.canCastFrostFire;
                    if (canCastFire || canCastFrostFire)
                        fireManaBar.renderer.enabled = true;
                }

                if (!canCastFrost)
                {
                    canCastFrost = player.canCastFrost;
                    bool canCastFrostFire = player.canCastFrostFire;
                    if (canCastFrost || canCastFrostFire)
                        frostManaBar.renderer.enabled = true;
                }
            }
        }

        #region helper functions

        void RegisterEventListener()
        {
            ManaSystem mana = PlayerController.Instance.Mana;
            mana.ManaChanged += OnManaChanged;
            registered = true;
            fireIncr = fireMaxWidth / mana.maxFireMana;
            frostIncr = frostMaxWidth / mana.maxFrostMana;
        }

        void OnManaChanged(int currentMana, MagicType manaType)
        {
            switch (manaType)
            {
                case MagicType.Fire:
                    AdjustBar(fireManaBar, fireIncr, currentMana);
                    break;
                case MagicType.Frost:
                    AdjustBar(frostManaBar, frostIncr, currentMana);
                    break;
            }
        }

        void AdjustBar(GameObject manaBar, float incr, int amount)
        {
            Vector3 newPos = Vector3.zero,
                    currentPos = manaBar.transform.position,
                    barBounds = manaBar.renderer.bounds.size,
                    scale = manaBar.transform.localScale;
            newPos.y = currentPos.y;
            newPos.z = currentPos.z;
            float oldHalfWidth = barBounds.x / 2f;
            newPos.x = currentPos.x - oldHalfWidth;
            scale.x = incr * amount;
            manaBar.transform.localScale = scale;
            float newHalfWidth = manaBar.renderer.bounds.size.x / 2f;
            newPos.x += newHalfWidth;
            manaBar.transform.position = newPos;
        }

        #endregion helper functions
    }
}
