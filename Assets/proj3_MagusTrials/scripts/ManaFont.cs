using UnityEngine;
using System.Collections;

namespace MagusTrials
{
    public class ManaFont : MonoBehaviour
    {
        public MagicType manaType;
        public int rechargeAmount = 1;
        public float rechargeDelay = 1f;

        private bool rechargingPlayer = false;
        private float nextRechargeTick = 0f;

        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("magus_player"))
            {
                rechargingPlayer = true;
            }
        }

        void OnTriggerExit(Collider collider)
        {
            if (collider.gameObject.CompareTag("magus_player"))
            {
                rechargingPlayer = false;
            }
        }

        void Update()
        {
            if (rechargingPlayer && Time.time >= nextRechargeTick)
            {
                nextRechargeTick = Time.time + rechargeDelay;
                switch (manaType)
                {
                    case MagicType.Fire:
                        PlayerController.Instance.Mana.CurrentFireMana += rechargeAmount;
                        break;
                    case MagicType.Frost:
                        PlayerController.Instance.Mana.CurrentFrostMana += rechargeAmount;
                        break;
                }
            }
        }
    }
}
