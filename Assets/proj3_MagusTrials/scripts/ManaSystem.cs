using UnityEngine;
using System.Collections;

namespace MagusTrials
{
    public enum MagicType
    {
        Fire,
        Frost,
        FrostFire
    }

    public class ManaSystem : MonoBehaviour
    {
        public delegate void ManaChangedHandler(int current, MagicType type);
        public event ManaChangedHandler ManaChanged = delegate { };

        public int maxFireMana = 100;
        public int maxFrostMana = 100;

        private int _currentFireMana;
        public int CurrentFireMana
        {
            get { return _currentFireMana; }
            set
            {
                _currentFireMana = value;
                _currentFireMana = Mathf.Clamp(_currentFireMana, 0, maxFireMana);
                ManaChanged(_currentFireMana, MagicType.Fire);
            }
        }

        private int _currentFrostMana;
        public int CurrentFrostMana
        {
            get { return _currentFrostMana; }
            set
            {
                _currentFrostMana = value;
                _currentFrostMana = Mathf.Clamp(_currentFrostMana, 0, maxFrostMana);
                ManaChanged(_currentFrostMana, MagicType.Frost);
            }
        }

        void Start()
        {
            _currentFireMana = maxFireMana;
            _currentFrostMana = maxFrostMana;
        }

        #region public functions

        public bool Cast(int amount, MagicType type)
        {
            if (HasMana(amount, type))
            {
                switch (type)
                {
                    case MagicType.Fire:
                        CurrentFireMana -= amount;
                        return true;
                    case MagicType.Frost:
                        CurrentFrostMana -= amount;
                        return true;
                    case MagicType.FrostFire:
                        CurrentFireMana -= amount;
                        CurrentFrostMana -= amount;
                        return true;
                }
            }
            return false;
        }

        public bool HasMana(int amount, MagicType type)
        {
            switch (type)
            {
                case MagicType.Fire:
                    return CurrentFireMana >= amount;
                case MagicType.Frost:
                    return CurrentFrostMana >= amount;
                case MagicType.FrostFire:
                    return CurrentFrostMana >= amount && CurrentFireMana >= amount;
            }

            return false;
        }

        #endregion public functions
    }
}
