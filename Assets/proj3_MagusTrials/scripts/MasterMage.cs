using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class MasterMage : MonoBehaviour
    {
        public GameObject text;

        void Start()
        {
            HideText();
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("magus_player"))
            {
                ShowText();
            }
        }

        void OnTriggerExit(Collider collider)
        {
            if (collider.gameObject.CompareTag("magus_player"))
            {
                HideText();
            }
        }

        void ShowText()
        {
            foreach (Transform child in text.transform)
            {
                child.renderer.enabled = true;
            }
        }
        
        void HideText()
        {
            foreach (Transform child in text.transform)
            {
                child.renderer.enabled = false;
            }
        }
    }
}