using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class MobVision : MonoBehaviour
    {
        public delegate void PlayerSightedHandler(Collider playerCollider);
        public event PlayerSightedHandler PlayerSighted = delegate { };
        public event PlayerSightedHandler LostPlayer = delegate { };

        void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("magus_player"))
                PlayerSighted(collider);
        }

        void OnTriggerExit(Collider collider)
        {
            if (collider.CompareTag("magus_player"))
                LostPlayer(collider);
        }
    }
}