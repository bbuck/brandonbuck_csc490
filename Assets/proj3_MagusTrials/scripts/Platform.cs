using UnityEngine;
using UnityRandom = UnityEngine.Random;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    [RequireComponent(typeof(MeshRenderer))]
    public class Platform : MonoBehaviour
    {
        public float speed = 3f;
        public Color frozenColor = Color.blue;
        public float freezeDuration = 1.5f;

        private Vector3 direction;
        private bool frozen = false;

        void OnEnable()
        {
            direction = (UnityRandom.Range(1, 20) <= 10 ? Vector3.right : Vector3.left);
        }

        void Update()
        {
            if (!frozen)
            {
                Vector3 position = transform.position;
                Vector3 destination = position + (direction * speed);
                position = Vector3.Lerp(position, destination, Time.deltaTime);
                transform.position = position;
            }
                
        }

        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("environment"))
            {
                direction = direction == Vector3.right ? Vector3.left : Vector3.right;
            }
        }

        #region helper functions

        void SetMaterialColor(Color color)
        {
            Material material = GetComponent<MeshRenderer>().material;
            if (material != null && material.shader.name == "Custom/ColorTint")
            {
                material.SetColor("_ColorTint", color);
                StartCoroutine("Unfreeze");
            }
        }

        IEnumerator Unfreeze()
        {
            yield return new WaitForSeconds(freezeDuration);
            frozen = false;
            SetMaterialColor(Color.white);
            StopCoroutine("Unfreeze");
        }

        #endregion helper functions

        #region public functions

        public void Freeze()
        {
            frozen = true;
            SetMaterialColor(frozenColor);
        }

        #endregion public functions
    }
}