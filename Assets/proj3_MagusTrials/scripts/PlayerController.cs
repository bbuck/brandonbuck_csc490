using UnityEngine;
using System.Collections;

namespace MagusTrials
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CharacterMotor))]
    [RequireComponent(typeof(SquarePlane))]
    [RequireComponent(typeof(Sprite))]
    [RequireComponent(typeof(HealthSystem))]
    [RequireComponent(typeof(ManaSystem))]
    public class PlayerController : MonoBehaviour
    {
        public float speed = 3f;
        public float castRechargeTime = .2f;
        public float invulnerabilityTime = .2f;
        public float takeDamageForce = 3f;
        public GameObject fireballPrefab;
        public GameObject frostSpikePrefab;
        public GameObject frostFirePrefab;
        public int frostFireCost = 9;
        public int fireManaCost = 8;
        public int frostManaCost = 12;

        public bool canCastFire = false;
        public bool canCastFrost = false;
        public bool canCastFrostFire = false;

        [HideInInspector]
        public HealthSystem Health { get; private set; }

        [HideInInspector]
        public ManaSystem Mana { get; private set; }

        [HideInInspector]
        public CharacterMotor Motor { get; private set; }

        public static PlayerController Instance { get; private set; }

        private SquarePlane plane;
        private Sprite sprite;
        private GameObject launchPoint;
        private float nextCast = 0f;
        private float invulnerableTill = 0f;
        private Vector3 hitDirection = Vector3.zero;
        private bool showCastingAnimation = false;

        void OnEnable()
        {
            if (Instance == null)
                Instance = this;
            plane = GetComponent<SquarePlane>();
            sprite = GetComponent<Sprite>();
            Motor = GetComponent<CharacterMotor>();
            launchPoint = transform.Find("spell_launch").gameObject;
            Health = GetComponent<HealthSystem>();
            Mana = GetComponent<ManaSystem>();
            sprite.FrameChanged += OnFrameChanged;
        }

        void OnDisable()
        {
            sprite.FrameChanged -= OnFrameChanged;
        }

        void Update()
        {
            Vector3 launchPointLocalPos = launchPoint.transform.localPosition,
                    launchPos = launchPoint.transform.position;
            if (hitDirection == Vector3.zero && Time.time > invulnerableTill)
            {
                Motor.targetVelocity.x = Input.GetAxis("Horizontal") * speed;
                if (Input.GetButton("Jump") && Motor.AbleToJump)
                    Motor.Jump();

                bool flipped = launchPoint.transform.localPosition.x < 0;
                bool castSpell = false;
                if (canCastFire && Input.GetButton("SpellSlot1") && Time.time > nextCast)
                {
                    if (Mana.Cast(fireManaCost, MagicType.Fire))
                    {
                        nextCast = Time.time + castRechargeTime;
                        GameObject fireball = PrefabManager.Instance.CreateInstance("Fireball", launchPos);
                        Fireball fball = fireball.GetComponent<Fireball>();
                        fball.Flip(flipped);
                        fball.Fire();
                        castSpell = true;
                    }
                }
                if (canCastFrost && Input.GetButton("SpellSlot2") && Time.time > nextCast)
                {
                    if (Mana.Cast(frostManaCost, MagicType.Frost))
                    {
                        nextCast = Time.time + castRechargeTime;
                        GameObject frostSpike = PrefabManager.Instance.CreateInstance("Frost Spike", launchPos);
                        FrostSpike fspike = frostSpike.GetComponent<FrostSpike>();
                        fspike.Flip(flipped);
                        fspike.Fire();
                        castSpell = true;
                    }
                }
                if (canCastFrostFire && Input.GetButton("SpellSlot3") && Time.time > nextCast)
                {
                    if (Mana.Cast(frostFireCost, MagicType.FrostFire))
                    {
                        nextCast = Time.time + castRechargeTime;
                        GameObject frostFireball = PrefabManager.Instance.CreateInstance("Frostfire Ball", launchPos);
                        FrostFireball ffball = frostFireball.GetComponent<FrostFireball>();
                        ffball.Flip(flipped);
                        ffball.Fire();
                        castSpell = true;
                    }
                }
                if (castSpell)
                {
                    sprite.AnimationPlaying = "Casting";
                    showCastingAnimation = true;
                }
            }
            else
            {
                Motor.Impulse(hitDirection * takeDamageForce);
                hitDirection = Vector3.zero;
            }

            if (!showCastingAnimation)
            {
                if (Motor.InAir && rigidbody.velocity.y < 0)
                    sprite.AnimationPlaying = "Falling";
                else if (Motor.InAir)
                    sprite.AnimationPlaying = "Jumping";
                else if (Motor.targetVelocity.x != 0)
                    sprite.AnimationPlaying = "Running";
                else
                    sprite.AnimationPlaying = "Standing";
            }

            if (Motor.targetVelocity.x > 0)
            {
                launchPointLocalPos.x = Mathf.Abs(launchPointLocalPos.x);
                plane.UVCoords.Flip = false;
                plane.GenerateMesh();
            }
            else if (Motor.targetVelocity.x < 0)
            {
                launchPointLocalPos.x = -Mathf.Abs(launchPointLocalPos.x);
                plane.UVCoords.Flip = true;
                plane.GenerateMesh();
            }
            launchPoint.transform.localPosition = launchPointLocalPos;
        }

        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("magus_enemy") && Time.time > invulnerableTill)
            {
                Health.Damage();
                hitDirection = collision.contacts[0].normal;
                invulnerableTill = Time.time + invulnerabilityTime;
            }

        }

        #region helper functions

        void OnFrameChanged(string frameName)
        {
            if (showCastingAnimation && frameName == "Casting.png")
            {
                showCastingAnimation = false;
            }
        }

        #endregion helper functions
    }
}
