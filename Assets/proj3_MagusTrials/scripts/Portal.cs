using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagusTrials
{
    public class Portal : MonoBehaviour 
    {
        public GameObject linkTo;

        void OnTriggerEnter(Collider collider)
        {
            if (linkTo == null)
                return;
            if (collider.gameObject.CompareTag("magus_player"))
            {
                Vector3 destination = linkTo.transform.position;
                collider.gameObject.transform.position = destination;
            }
        }
    }
}