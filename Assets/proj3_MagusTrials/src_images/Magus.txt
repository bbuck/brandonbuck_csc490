{"frames": {

"Blaze1.png":
{
	"frame": {"x":536,"y":24,"w":46,"h":52},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":30,"y":7,"w":46,"h":52},
	"sourceSize": {"w":100,"h":100}
},
"Blaze2.png":
{
	"frame": {"x":790,"y":2,"w":54,"h":56},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":31,"y":3,"w":54,"h":56},
	"sourceSize": {"w":100,"h":100}
},
"BossFireball1.png":
{
	"frame": {"x":484,"y":68,"w":44,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":4,"w":44,"h":40},
	"sourceSize": {"w":50,"h":50}
},
"BossFireball2.png":
{
	"frame": {"x":584,"y":24,"w":42,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":4,"w":42,"h":40},
	"sourceSize": {"w":50,"h":50}
},
"Bricks.png":
{
	"frame": {"x":898,"y":15,"w":50,"h":50},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":50,"h":50},
	"sourceSize": {"w":50,"h":50}
},
"Casting.png":
{
	"frame": {"x":742,"y":144,"w":30,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":0,"w":30,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"Crystal1.png":
{
	"frame": {"x":876,"y":157,"w":20,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":1,"w":20,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"Crystal2.png":
{
	"frame": {"x":998,"y":166,"w":20,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":1,"w":20,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"Crystal3.png":
{
	"frame": {"x":976,"y":148,"w":20,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":23,"y":1,"w":20,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"Crystal4.png":
{
	"frame": {"x":998,"y":116,"w":20,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":13,"y":1,"w":20,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"EmptyBar.png":
{
	"frame": {"x":688,"y":2,"w":100,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":100,"h":20},
	"sourceSize": {"w":100,"h":20}
},
"EmptyHeart.png":
{
	"frame": {"x":484,"y":24,"w":50,"h":42},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":5,"w":50,"h":42},
	"sourceSize": {"w":50,"h":50}
},
"Falling1.png":
{
	"frame": {"x":844,"y":109,"w":30,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":2,"w":30,"h":48},
	"sourceSize": {"w":40,"h":50}
},
"Falling2.png":
{
	"frame": {"x":912,"y":83,"w":30,"h":50},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":0,"w":30,"h":50},
	"sourceSize": {"w":40,"h":50}
},
"FireBar.png":
{
	"frame": {"x":586,"y":2,"w":100,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":100,"h":20},
	"sourceSize": {"w":100,"h":20}
},
"FireBarrier1.png":
{
	"frame": {"x":670,"y":100,"w":36,"h":100},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":0,"w":36,"h":100},
	"sourceSize": {"w":50,"h":100}
},
"FireBarrier2.png":
{
	"frame": {"x":484,"y":110,"w":38,"h":100},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":38,"h":100},
	"sourceSize": {"w":50,"h":100}
},
"FireParticle.png":
{
	"frame": {"x":846,"y":15,"w":50,"h":50},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":50,"h":50},
	"sourceSize": {"w":50,"h":50}
},
"Fireball1.png":
{
	"frame": {"x":944,"y":134,"w":30,"h":16},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":2,"w":30,"h":16},
	"sourceSize": {"w":30,"h":20}
},
"Fireball2.png":
{
	"frame": {"x":944,"y":116,"w":30,"h":16},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":2,"w":30,"h":16},
	"sourceSize": {"w":30,"h":20}
},
"FireballCast.png":
{
	"frame": {"x":570,"y":96,"w":10,"h":16},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":20,"y":2,"w":10,"h":16},
	"sourceSize": {"w":30,"h":20}
},
"FireballHit.png":
{
	"frame": {"x":606,"y":166,"w":18,"h":20},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":0,"w":18,"h":20},
	"sourceSize": {"w":30,"h":20}
},
"FrostBar.png":
{
	"frame": {"x":484,"y":2,"w":100,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":100,"h":20},
	"sourceSize": {"w":100,"h":20}
},
"FrostCast1.png":
{
	"frame": {"x":1006,"y":2,"w":14,"h":18},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":11,"y":12,"w":14,"h":18},
	"sourceSize": {"w":50,"h":50}
},
"FrostCast2.png":
{
	"frame": {"x":1006,"y":22,"w":12,"h":14},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":11,"y":14,"w":12,"h":14},
	"sourceSize": {"w":50,"h":50}
},
"FrostCrystal1.png":
{
	"frame": {"x":976,"y":98,"w":20,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":1,"w":20,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"FrostCrystal2.png":
{
	"frame": {"x":820,"y":110,"w":20,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":1,"w":20,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"FrostCrystal3.png":
{
	"frame": {"x":822,"y":60,"w":20,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":23,"y":1,"w":20,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"FrostCrystal4.png":
{
	"frame": {"x":1002,"y":66,"w":20,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":13,"y":1,"w":20,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"FrostHitWall.png":
{
	"frame": {"x":1006,"y":38,"w":10,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":14,"y":5,"w":10,"h":26},
	"sourceSize": {"w":50,"h":50}
},
"FrostSpike1.png":
{
	"frame": {"x":786,"y":60,"w":34,"h":16},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":16,"w":34,"h":16},
	"sourceSize": {"w":50,"h":50}
},
"FrostSpike2.png":
{
	"frame": {"x":626,"y":126,"w":36,"h":14},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":15,"w":36,"h":14},
	"sourceSize": {"w":50,"h":50}
},
"FrostSpike3.png":
{
	"frame": {"x":530,"y":78,"w":40,"h":16},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":12,"w":40,"h":16},
	"sourceSize": {"w":50,"h":50}
},
"FrostfireBarrier1.png":
{
	"frame": {"x":710,"y":24,"w":36,"h":100},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":0,"w":36,"h":100},
	"sourceSize": {"w":50,"h":100}
},
"FrostfireBarrier2.png":
{
	"frame": {"x":530,"y":96,"w":38,"h":100},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":38,"h":100},
	"sourceSize": {"w":50,"h":100}
},
"FrostfireScroll.png":
{
	"frame": {"x":910,"y":135,"w":26,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":11,"y":2,"w":26,"h":48},
	"sourceSize": {"w":50,"h":50}
},
"Frostfireball1.png":
{
	"frame": {"x":944,"y":98,"w":30,"h":16},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":2,"w":30,"h":16},
	"sourceSize": {"w":30,"h":20}
},
"Frostfireball2.png":
{
	"frame": {"x":708,"y":168,"w":30,"h":16},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":2,"w":30,"h":16},
	"sourceSize": {"w":30,"h":20}
},
"FrostfireballCast.png":
{
	"frame": {"x":572,"y":78,"w":10,"h":16},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":20,"y":2,"w":10,"h":16},
	"sourceSize": {"w":30,"h":20}
},
"FrostfireballHit1.png":
{
	"frame": {"x":606,"y":144,"w":18,"h":20},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":0,"w":18,"h":20},
	"sourceSize": {"w":30,"h":20}
},
"GameOver.png":
{
	"frame": {"x":2,"y":144,"w":194,"h":190},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":7,"w":194,"h":190},
	"sourceSize": {"w":200,"h":200}
},
"Heart.png":
{
	"frame": {"x":950,"y":54,"w":50,"h":42},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":5,"w":50,"h":42},
	"sourceSize": {"w":50,"h":50}
},
"IceBarrier.png":
{
	"frame": {"x":628,"y":24,"w":40,"h":100},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":0,"w":40,"h":100},
	"sourceSize": {"w":50,"h":100}
},
"IceGolemAttacking1.png":
{
	"frame": {"x":584,"y":66,"w":40,"h":76},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":40,"h":76},
	"sourceSize": {"w":50,"h":80}
},
"IceGolemAttacking2.png":
{
	"frame": {"x":524,"y":198,"w":34,"h":76},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":34,"h":76},
	"sourceSize": {"w":50,"h":80}
},
"IceGolemStanding.png":
{
	"frame": {"x":570,"y":144,"w":34,"h":76},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":34,"h":76},
	"sourceSize": {"w":50,"h":80}
},
"IceGolemWalking1.png":
{
	"frame": {"x":626,"y":142,"w":34,"h":76},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":34,"h":76},
	"sourceSize": {"w":50,"h":80}
},
"IceGolemWalking2.png":
{
	"frame": {"x":748,"y":24,"w":36,"h":76},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":1,"w":36,"h":76},
	"sourceSize": {"w":50,"h":80}
},
"IceGolemWalking3.png":
{
	"frame": {"x":670,"y":24,"w":38,"h":74},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":0,"w":38,"h":74},
	"sourceSize": {"w":50,"h":80}
},
"IceLord.png":
{
	"frame": {"x":198,"y":144,"w":76,"h":74},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":13,"y":14,"w":76,"h":74},
	"sourceSize": {"w":100,"h":100}
},
"IceParticle.png":
{
	"frame": {"x":954,"y":2,"w":50,"h":50},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":50,"h":50},
	"sourceSize": {"w":50,"h":50}
},
"Jump1.png":
{
	"frame": {"x":782,"y":124,"w":30,"h":44},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":1,"w":30,"h":44},
	"sourceSize": {"w":40,"h":50}
},
"Jump2.png":
{
	"frame": {"x":786,"y":78,"w":32,"h":44},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":2,"w":32,"h":44},
	"sourceSize": {"w":40,"h":50}
},
"MasterMage.png":
{
	"frame": {"x":878,"y":107,"w":30,"h":48},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":0,"w":30,"h":48},
	"sourceSize": {"w":40,"h":50}
},
"Pedastool.png":
{
	"frame": {"x":912,"y":67,"w":32,"h":14},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":9,"y":36,"w":32,"h":14},
	"sourceSize": {"w":50,"h":50}
},
"PlatformFrozen.png":
{
	"frame": {"x":900,"y":2,"w":52,"h":11},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":14,"w":52,"h":11},
	"sourceSize": {"w":52,"h":25}
},
"PlatformNormal.png":
{
	"frame": {"x":846,"y":2,"w":52,"h":11},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":14,"w":52,"h":11},
	"sourceSize": {"w":52,"h":25}
},
"Standing1.png":
{
	"frame": {"x":844,"y":67,"w":32,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":0,"w":32,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"Standing2.png":
{
	"frame": {"x":708,"y":126,"w":32,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":0,"w":32,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"Standing3.png":
{
	"frame": {"x":878,"y":67,"w":32,"h":38},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":2,"w":32,"h":38},
	"sourceSize": {"w":40,"h":40}
},
"Title1.png":
{
	"frame": {"x":286,"y":2,"w":196,"h":176},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":4,"w":196,"h":176},
	"sourceSize": {"w":200,"h":200}
},
"UserInterface.png":
{
	"frame": {"x":2,"y":2,"w":282,"h":140},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":4,"w":282,"h":140},
	"sourceSize": {"w":300,"h":150}
},
"Walking2.png":
{
	"frame": {"x":748,"y":102,"w":32,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":0,"w":32,"h":40},
	"sourceSize": {"w":40,"h":40}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "Magus.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:936be2500a95bff84fd3a082e3bf3727$"
}
}
