using UnityEngine;
using UnityEditor;
using System.Collections;

public class CreateSpritePlane
{
    [MenuItem("GameObject/Create Other/Sprite Plane")]
    static void Create()
    {
        GameObject go = new GameObject();
        go.name = "Sprite Plane";
        SquarePlane squarePlane = go.AddComponent<SquarePlane>();
        go.AddComponent<Sprite>();
        squarePlane.GenerateMesh();
        MeshRenderer meshRenderer = go.GetComponent<MeshRenderer>();
        meshRenderer.material = new Material(Shader.Find("Custom/ColorTint"));
        Vector3 goScale = go.transform.localScale;
        Vector3 colliderScale = new Vector3(goScale.x, goScale.y, 0.5f);
        go.AddComponent<BoxCollider>().size = colliderScale;
        Selection.activeGameObject = go;
        SceneView sceneView = Utils.GetSceneView();
        if (sceneView != null)
            go.transform.position = sceneView.camera.transform.position + (sceneView.camera.transform.forward * Utils.GameObjectCameraDistance);
    }
}
