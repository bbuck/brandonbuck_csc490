using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

[CustomEditor(typeof(HealthSystem))]
public class HealthSystemEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        HealthSystem health = target as HealthSystem;
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Current Health", health.CurrentHealth.ToString());
        EditorGUILayout.EndHorizontal();
    }
}
