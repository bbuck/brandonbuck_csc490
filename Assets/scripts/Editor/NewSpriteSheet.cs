using UnityEngine;
using UnityEditor;
using System.Collections;

public class NewSpriteSheet {
	
	[MenuItem("Assets/Create Other/Sprite Sheet")]
	public static void CreateSpriteSheet() {
		SpriteSheet sheet = ScriptableObject.CreateInstance<SpriteSheet>();
		string selectedPath = AssetDatabase.GetAssetPath(Selection.activeObject);
		string path = "";
		if (selectedPath != null && selectedPath.Length > 0)
			path = string.Format("{0}/NewSpriteSheet.asset", selectedPath);
		else
			path = "Assets/NewSpriteSheet.asset";
		AssetDatabase.CreateAsset(sheet, path);
		Selection.activeObject = sheet;
		AssetDatabase.SaveAssets();
	}
}
