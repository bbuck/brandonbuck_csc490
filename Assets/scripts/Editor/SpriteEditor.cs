using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Sprite))]
[CanEditMultipleObjects]
public class SpriteEditor : Editor {
	
	[SerializeField]
	private bool showAnimations = false;
	
	[SerializeField]
	private bool showSelectedAnimation = false;
	
	[SerializeField]
	private int selectedAnimation = -1;
	
	[SerializeField]
	private int currentNewAnimation = 0;

    private Sprite _sprite;
    public Sprite TargetSprite
    {
        get
        {
            if (_sprite == null)
                _sprite = target as Sprite;
            return _sprite;
        }
    }

    private bool _setIsInSceneView = false;
    private bool _isInSceneView = false;
    private bool InSceneView
    {
        get
        {
            if (!_setIsInSceneView)
            {
                _setIsInSceneView = true;
                foreach (Transform trans in Selection.transforms)
                {
                    if (TargetSprite.transform == trans)
                    {
                        _isInSceneView = true;
                        break;
                    }
                }
            }
            return _isInSceneView;
        }
    }

	private int playingAnimation = -1;
	private int currentPlayingFrame = 0;
	private float nextFrame = 0f;
	private DateTime startTime = DateTime.Now;
	
	public override void OnInspectorGUI() {
		TimeSpan timeDiff = (DateTime.Now - startTime);
		float elapsed = (float)timeDiff.TotalSeconds;
		DrawDefaultInspector();

		if (InSceneView && playingAnimation > -1 && playingAnimation < TargetSprite.animationList.Count) 
        {
			if (elapsed >= nextFrame) 
            {
				SpriteAnimation current = TargetSprite.animationList[playingAnimation];
				string frameName = current.Frames[currentPlayingFrame++];
				TargetSprite.RenderFrame(frameName);
				if (currentPlayingFrame >= current.Frames.Count)
					currentPlayingFrame = 0;
				nextFrame = elapsed + current.Delay;
			}
		}

        EditorGUILayout.BeginHorizontal();
        TargetSprite.AnimationPlaying = EditorGUILayout.TextField("Animation Playing", TargetSprite.AnimationPlaying);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        float newScale = EditorGUILayout.FloatField("Scale", TargetSprite.scale);
        if (newScale != TargetSprite.scale)
        {
            Undo.RegisterUndo(TargetSprite, "Undo scale change.");
            TargetSprite.scale = newScale;
            RerenderFrame(true);
        }
        EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		showAnimations = EditorGUILayout.Foldout(showAnimations, "Animations");
		if (GUILayout.Button("Create New Animation")) 
        {
			SpriteAnimation newAnim = new SpriteAnimation();
			string name = "Unnamed" + currentNewAnimation++;
            Undo.RegisterUndo(TargetSprite, "Undo add new animation.");
			TargetSprite.keyList.Add(name);
			TargetSprite.animationList.Add(newAnim);
			selectedAnimation = TargetSprite.keyList.Count - 1;
            showAnimations = true;
		}
		EditorGUILayout.EndHorizontal();
		if (showAnimations) 
        {
			for (int i = 0, len = TargetSprite.keyList.Count; i < len; i++)
            {
				string key = TargetSprite.keyList[i];
				EditorGUILayout.BeginHorizontal();
				string newKey = EditorGUILayout.TextField(key);
				if (GUILayout.Button("Edit")) 
                {
					selectedAnimation = i;
					showSelectedAnimation = false;
				}
                if (InSceneView)
                {
                    if (GUILayout.Button("\u25ba", GUILayout.Width(20))) // Play animation
                    {
                        currentPlayingFrame = 0;
                        playingAnimation = i;
                        nextFrame = elapsed + TargetSprite.animationList[i].Delay;
                    }
                }		
				if (GUILayout.Button("-", GUILayout.Width(20))) 
                {
                    Undo.RegisterUndo(TargetSprite, "Undo remove animation.");
					TargetSprite.keyList.RemoveAt(i);
					TargetSprite.animationList.RemoveAt(i);
				}
                
                if (newKey != key)
                {
                    Undo.RegisterUndo(TargetSprite, "Undo animation key change.");
                    TargetSprite.keyList[i] = newKey;
                }
				EditorGUILayout.EndHorizontal();
			}
		}
        EditorGUILayout.Space();
		if (selectedAnimation > -1) 
        {
			string animName = TargetSprite.keyList[selectedAnimation];
			showSelectedAnimation = EditorGUILayout.Foldout(showSelectedAnimation, string.Format("Selected Animation (\"{0}\")", animName));
			if (showSelectedAnimation) 
            {
                SpriteAnimation anim = TargetSprite.animationList[selectedAnimation];
                EditorGUILayout.BeginHorizontal();
				float fps = EditorGUILayout.FloatField("Frames Per Second", anim.FPS);
                if (fps != anim.FPS)
                {
                    Undo.RegisterUndo(TargetSprite, "Undo FPS change.");
                    anim.FPS = fps;
                }
                EditorGUILayout.EndHorizontal();
                if (TargetSprite.spriteSheet != null)
                {
                    string[] frameNames = TargetSprite.spriteSheet.Keys;
                    for (int i = 0, len = anim.Frames.Count; i < len; i++)
                    {
                        EditorGUILayout.BeginHorizontal();
                        string frameName = anim.Frames[i];
                        int frameNameIndex = 0;
                        for (int fIndex = 0, frameNamesLength = frameNames.Length; fIndex < frameNamesLength; fIndex++)
                        {
                            if (frameNames[fIndex] == frameName)
                            {
                                frameNameIndex = fIndex;
                                break;
                            }
                        }
                        int newFrameNameIndex = EditorGUILayout.Popup(frameNameIndex, frameNames);
                        string newFrameName = frameNames[newFrameNameIndex];
                        if (frameName != newFrameName)
                        {
                            Undo.RegisterUndo(TargetSprite, "Undo frame change.");
                            RenderFrame(newFrameName, true);
                            playingAnimation = -1;
                        }
                        anim.Frames[i] = newFrameName;
                        if (InSceneView)
                        {
                            if (GUILayout.Button("Render"))
                            {
                                RenderFrame(newFrameName, true);
                                playingAnimation = -1;
                            }
                        }
                        if (GUILayout.Button("-", GUILayout.Width(20)))
                        {
                            anim.Frames.RemoveAt(i);
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    if (GUILayout.Button("New Frame"))
                    {
                        Undo.RegisterUndo(TargetSprite, "Undo new frame.");
                        anim.Frames.Add("New Frame");
                    }
                }
                else
                    EditorGUILayout.LabelField("No Sprite Sheet has been Set");
			}
		}
		
		if (GUILayout.Button("Remove All Animations")) 
        {
            Undo.RegisterUndo(TargetSprite, "Undo remove all animations.");
			TargetSprite.keyList = new List<string>();
			TargetSprite.animationList = new List<SpriteAnimation>();
			selectedAnimation = -1;
			showSelectedAnimation = false;
			showAnimations = false;
		}
        if (playingAnimation >= 0)
        {
            if (GUILayout.Button("Stop Animation"))
            {
                currentPlayingFrame = 0;
                playingAnimation = -1;
                nextFrame = 0f;
            }
        }
		
		Repaint();
	}

    void RerenderFrame(bool force)
    {
        if (InSceneView)
        {
            TargetSprite.RerenderFrame(force);
        }
    }

    void RenderFrame(string frameName, bool force)
    {
        if (InSceneView)
            TargetSprite.RenderFrame(frameName, force);
    }
}
