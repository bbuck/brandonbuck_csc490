using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(SquarePlane))]
public class SquarePlaneEditor : Editor {
	private bool showUvCoords = false;
	
	public override void OnInspectorGUI() {
		DrawDefaultInspector();
		
		SquarePlane plane = target as SquarePlane;
		
		showUvCoords = EditorGUILayout.Foldout(showUvCoords, "UV Coordinates");
		if (showUvCoords) {
			if (plane.UVCoords == null)
				plane.UVCoords = new UVCoordinates();
			bool newFlip = EditorGUILayout.Toggle("Flip", plane.UVCoords.Flip);
            if (newFlip != plane.UVCoords.Flip)
            {
                Undo.RegisterUndo(plane, "Undo flip UV Coordinates");
                plane.UVCoords.Flip = newFlip;
            }
			Vector2 newTopLeft = EditorGUILayout.Vector2Field("Top Left", plane.UVCoords.TopLeft);
            if (newTopLeft != plane.UVCoords.TopLeft)
            {
                Undo.RegisterUndo(plane, "Undo change top left UV coordinate");
                plane.UVCoords.TopLeft = newTopLeft;
            }
			Vector2 newBottomLeft = EditorGUILayout.Vector2Field("Bottom Left", plane.UVCoords.BottomLeft);
            if (newBottomLeft != plane.UVCoords.BottomLeft)
            {
                Undo.RegisterUndo(plane, "Undo change bottom left UV coordinate");
                plane.UVCoords.BottomLeft = newBottomLeft;
            }
			Vector2 newBottomRight = EditorGUILayout.Vector2Field("Bottom Right", plane.UVCoords.BottomRight);
            if (newBottomRight != plane.UVCoords.BottomRight)
            {
                Undo.RegisterUndo(plane, "Undo change bottom right UV coordinate");
                plane.UVCoords.BottomRight = newBottomRight;
            }
			Vector2 newTopRight = EditorGUILayout.Vector2Field("Top Right", plane.UVCoords.TopRight);
            if (newTopRight != plane.UVCoords.TopRight)
            {
                Undo.RegisterUndo(plane, "Undo change top right UV coordinate");
                plane.UVCoords.TopRight = newTopRight;
            }
		}
		
		if (GUILayout.Button("Generate Mesh")) {
			plane.GenerateMesh();	
		}
		
		Repaint();
	}
}
