using UnityEngine;
using UnityEditor;
using System.Collections;

public class ViewSnap
{
    [MenuItem("View/Front %1")]
    static void FrontView()
    {
        LookAt(Vector3.back);
    }

    [MenuItem("View/Back %2")]
    static void BackView()
    {
        LookAt(Vector3.forward);
    }

    [MenuItem("View/Left %3")]
    static void LeftView()
    {
        LookAt(Vector3.right);
    }

    [MenuItem("View/Right %4")]
    static void RightView()
    {
        LookAt(Vector3.left);
    }

    [MenuItem("View/Top %5")]
    static void TopView()
    {
        LookAt(Vector3.down);
    }

    [MenuItem("View/Bottom %6")]
    static void BottomView()
    {
        LookAt(Vector3.up);
    }

    [MenuItem("View/Swap Type %7")]
    static void SwapViewType()
    {
        Utils.GetSceneView().orthographic = !Utils.GetSceneView().orthographic;
    }

    static void LookAt(Vector3 direction)
    {
        SceneView sView = Utils.GetSceneView();
        sView.orthographic = true;
        sView.LookAt(sView.pivot, Quaternion.LookRotation(direction));
    }
}
