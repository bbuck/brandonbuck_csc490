using UnityEngine;
using System.Collections;

public class HealthSystem : MonoBehaviour {
	
	public delegate void HealthChangedHandler(int amount, int currentHealth);
	public event HealthChangedHandler HealthChanged;
	
	public int maxHealth = 3;
	
	public virtual int TotalHealth {
		get {
			return maxHealth;
		}
	}
	
	public int CurrentHealth {get; protected set;}
	
	void Awake() {
		OnAwake();	
	}
	
	#region event functions
	
	protected void OnHealthChanged(int amount, int currentHealth) {
		if (HealthChanged != null) {
			HealthChanged(amount, currentHealth);
		}
	}
	
	#endregion event functions
	
	#region helper functions
	
	protected virtual void OnAwake() {
        SetCurrentHealth(maxHealth, true);
	}
	
	#endregion helper functions
	
	#region public functions

    public virtual void SetCurrentHealth(int current, bool silent)
    {
        int change = current - CurrentHealth;
        CurrentHealth = current;
        if (!silent)
            OnHealthChanged(change, CurrentHealth);
    }

	public virtual bool IsDead() {
		return CurrentHealth <= 0;
	}
	
	public void Damage() {
		Damage(1);	
	}
	
	public virtual void Damage(int amount) {
		CurrentHealth -= amount;
		OnHealthChanged(amount, CurrentHealth);
	}
	
	public void Heal() {
		Heal(1);
	}
	
	public virtual void Heal(int amount) {
		CurrentHealth += amount;
		if (CurrentHealth > maxHealth)
			CurrentHealth = maxHealth;
		OnHealthChanged(amount, CurrentHealth);
	}
	
	#endregion public functions
}
