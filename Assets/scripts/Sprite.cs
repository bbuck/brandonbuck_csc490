using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
[RequireComponent(typeof(SquarePlane))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class Sprite : MonoBehaviour
{
    public delegate void FrameChangedHandler(string frameName);
    public event FrameChangedHandler FrameChanged = delegate { };

    #region properties

    public SpriteSheet spriteSheet;
    public bool resizeCollider = true;

	[SerializeField]
    [HideInInspector]
	public float scale = 1f;
	
	[SerializeField]
	[HideInInspector]
	public List<string> keyList = new List<string>();
	
	[SerializeField]
	[HideInInspector]
	public List<SpriteAnimation> animationList = new List<SpriteAnimation>();
	
	private Dictionary<string, SpriteAnimation> animations = new Dictionary<string, SpriteAnimation>(); 

	[SerializeField]
    [HideInInspector]
	private string _animationPlaying;
	public string AnimationPlaying 
    { 
		get { return _animationPlaying; } 
		set 
        {
			if (_animationPlaying != value) 
            {
				_animationPlaying = value;
				currentFrame = 0;
			}
		}
	}
	
	private SquarePlane _plane;
	private SquarePlane MeshPlane 
    {
		get 
        {
			if (_plane == null)
				_plane = GetComponent<SquarePlane>();
			return _plane;
		}
		set 
        {
			_plane = value;	
		}
	}

	private int currentFrame = 0;
    private string previousFrameName;
    private bool setMaterial = false;

    #endregion properties

    void OnEnable() 
    {
		animations = new Dictionary<string, SpriteAnimation>();
		for (int i = 0, len = keyList.Count; i < len; i++) 
        {
			animations[keyList[i]] = animationList[i];	
		}
	}
	
	void Start() 
    {
		MeshPlane = GetComponent<SquarePlane>();
        SetMaterial();
        if (AnimationPlaying == null)
            AnimationPlaying = "None";
		StartCoroutine("Animate");
	}

    #region helper functions

    void SetMaterial()
    {
        if (setMaterial)
            return;
        if (spriteSheet != null && spriteSheet.texture != null)
        {
            Material mat = new Material(Shader.Find("Custom/ColorTint"));
            mat.SetTexture("_MainTex", spriteSheet.texture);
            mat.SetColor("_ColorTint", Color.white);
            renderer.sharedMaterial = mat;
            setMaterial = true;
        }
    }

    IEnumerator Animate()
    {
        while (true)
        {
            if (animations.ContainsKey(AnimationPlaying))
            {
                SpriteAnimation animation = animations[AnimationPlaying];

                if (animation.Frames.Count == 0)
                {
                    yield return new WaitForSeconds(animation.Delay);
                    continue;
                }

                string frameName = animation.Frames[currentFrame];
                //print(string.Format("Current Frame: {0}", frameName));

                RenderFrame(frameName);

                if (animation.Frames.Count > 1)
                {
                    currentFrame++;
                    if (currentFrame >= animation.Frames.Count)
                        currentFrame = 0;
                }

                yield return new WaitForSeconds(animation.Delay);
            }
            else
                yield return new WaitForSeconds(.1f);
        }
    }

    #endregion helper functions

    #region public functions

    public void RerenderFrame()
    {
        RerenderFrame(false);
    }

    public void RerenderFrame(bool forceRender)
    {
        if (previousFrameName == null)
            return;

        RenderFrame(previousFrameName, forceRender);
    }

    public void RenderFrame(string frameName)
    {
        RenderFrame(frameName, false);
    }

    public void RenderFrame(string frameName, bool forceRender) 
    {
		if (!spriteSheet.ContainsKey(frameName) || (previousFrameName == frameName && !forceRender))
			return;
        SetMaterial();

        FrameChanged(frameName);
        previousFrameName = frameName;
		SpriteInfo data = spriteSheet[frameName];

		float uvX = data.UVPosition.x,
			  uvY = data.UVPosition.y,
		      uvWidth = data.UVSize.x,
			  uvHeight = data.UVSize.y;
		
		float width = uvWidth, 
              height = uvHeight;
		if (data.Size.x > data.Size.y) 
        {
			float factor = scale / data.Size.x;
			width = scale;
			height = data.Size.y * factor;
		}
		else 
        {
			float factor = scale / data.Size.y;
			height = scale;
			width = data.Size.x * factor;	
		}
		
		MeshPlane.width = width;
		MeshPlane.height = height;
        if (resizeCollider)
        {
            BoxCollider boxCollider = GetComponent<BoxCollider>();
            if (boxCollider != null)
                boxCollider.size = new Vector3(width, height, 1f);
            else
            {
                CapsuleCollider capCollider = GetComponent<CapsuleCollider>();
                if (capCollider != null)
                {
                    capCollider.height = height;
                    capCollider.radius = width * 0.5f;
                }
            }
        }
		
		MeshPlane.UVCoords.TopLeft = new Vector2(uvX, uvY);
		MeshPlane.UVCoords.BottomLeft = new Vector2(uvX, uvY - uvHeight);
		MeshPlane.UVCoords.BottomRight = new Vector2(uvX + uvWidth, uvY - uvHeight);
		MeshPlane.UVCoords.TopRight = new Vector2(uvX + uvWidth, uvY);
		
		MeshPlane.GenerateMesh();
	}

    #endregion public functions
}
