using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SpriteAnimation {
	[SerializeField]
	public List<string> Frames = new List<string>();
	
	[SerializeField]
	private float _fps;
	public float FPS { 
		get { return _fps; } 
		set {
			_fps = value;
		}
	}
	
	public float Delay { 
		get {
			return 1 / FPS; 
		}
	}
	
	public SpriteAnimation() {
		Frames = new List<string>();
		FPS = 30f;
	}
}
