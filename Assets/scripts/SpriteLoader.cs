using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SpriteLoader : MonoBehaviour 
{
	private static SpriteLoader _instance;
	public static SpriteLoader Instance {
		get {
			if (_instance == null) {
				GameObject go = new GameObject();
				go.name = "SpriteLoader";
				_instance = go.AddComponent<SpriteLoader>();
			}
			return _instance;
		}
	}
	
	private Dictionary<string, IDictionary> _spriteData = new Dictionary<string, IDictionary>();	
	public Dictionary<string, IDictionary> SpriteData { 
		get { return _spriteData; }
		private set { _spriteData = value; }
	}
	
	public IDictionary LoadData(TextAsset jsonFile) {
		string name = jsonFile.name;
		if (SpriteData.ContainsKey(name))
			return SpriteData[name];
		SpriteData[name] = jsonFile.text.hashtableFromJson() as IDictionary;
		return SpriteData[name];
	}
}
