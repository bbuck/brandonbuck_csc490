using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteInfo {
	public Vector2 UVPosition { get; set; }
	public Vector2 UVSize { get; set; }
	public Vector2 Size { get; set; }
	public Vector2 Position { get; set; }
}

public class SpriteSheet : ScriptableObject {
	
	[SerializeField]
	private TextAsset _spriteDataJson;
	public TextAsset SpriteDataJson {
		get {
			return _spriteDataJson;
		}
		set {
			string oldName = _spriteDataJson.name;
			_spriteDataJson = value;
			if (oldName != value.name)
				ProcessSpriteJson();
		}
	}
	
	[SerializeField]
	public Texture2D texture;
	
	private IDictionary _spriteData;
	private IDictionary SpriteData {
		get {
			if (_spriteData == null)
				ProcessSpriteJson();
			return _spriteData;
		}
		set {
			_spriteData = value;	
		}
	}

    public string[] Keys {
        get {
            ICollection keyCollection = SpriteData.Keys;
            string[] keyList = new string[keyCollection.Count];
            keyCollection.CopyTo(keyList, 0);

            return keyList;
        }
    }

	[SerializeField]
	[HideInInspector]
	private Vector2 metaSize;
	
	private Dictionary<string, SpriteInfo> spriteDict = new Dictionary<string, SpriteInfo>();
	
	#region public methods
	
	public SpriteInfo this[string name] {
		get {
			if (SpriteData == null)
				ProcessSpriteJson();
			return GetSpriteInfo(name);
		}	
	}

	public bool ContainsKey(string key) {
        bool contains = spriteDict.ContainsKey(key);
		if (contains)
			return contains;
		
		IDictionary sprite = SpriteData[key] as IDictionary;
		return sprite != null;
	}
	
	public SpriteInfo GetSpriteInfo(string name) {
		if (spriteDict.ContainsKey(name))
			return spriteDict[name];
		
		IDictionary sprite = SpriteData[name] as IDictionary;
		IDictionary frame = sprite["frame"] as IDictionary;
		double? x, y, width, height;
		x = frame["x"] as double?;
		y = frame["y"] as double?;
		width = frame["w"] as double?;
		height = frame["h"] as double?;
		
		float uvX = (float)(x.Value / metaSize.x);
		float uvY = (float)(1f - (y.Value / metaSize.y));
		float uvWidth = (float)(width.Value / metaSize.x);
		float uvHeight = (float)(height.Value / metaSize.y);
		
		SpriteInfo data = new SpriteInfo();
		data.Size = new Vector2((float)width.Value, (float)height.Value);
		data.Position = new Vector2((float)x.Value, (float)y.Value);
		data.UVSize = new Vector2(uvWidth, uvHeight);
		data.UVPosition = new Vector2(uvX, uvY);
		
		spriteDict[name] = data;
		
		return data;
	}
	
	#endregion public methods
	
	#region helper methods
	
	void ProcessSpriteJson() {
		IDictionary middleMan = SpriteDataJson.text.hashtableFromJson() as IDictionary;
		IDictionary meta = middleMan["meta"] as IDictionary;
		IDictionary size = meta["size"] as IDictionary;
		SpriteData = middleMan["frames"] as IDictionary;
		double? width, height;
		width = size["w"] as double?;
		height = size["h"] as double?;
		metaSize = new Vector2((float)width.Value, (float)height.Value);
	}
	
	#endregion helper methods
}
